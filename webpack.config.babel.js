import webpack from 'webpack'
import HtmlWebpackPlugin from 'html-webpack-plugin'
import ExtractTextPlugin from 'extract-text-webpack-plugin'
import path from 'path'

let isProduction = process.env.NODE_ENV === 'production' // true or false
let cssDev = ['style-loader', 'css-loader', 'stylus-loader']
let cssProduction = ExtractTextPlugin.extract({
  fallback: 'style-loader',
  use: ['css-loader', 'stylus-loader']
})
let cssConfig = isProduction ? cssProduction : cssDev

module.exports = {
  entry: path.resolve(__dirname, 'src/assets/app.js'),
  output: {
    path: path.resolve(__dirname, 'public'),
    publicPath: path.resolve(__dirname, 'public'),
    filename: 'app.bundle.js'
  },
  devtool: 'source-map',
  node: {
    console: false,
    fs: 'empty',
    net: 'empty',
    tls: 'empty'
  },
  resolve: {
    modules: [path.resolve(__dirname, 'src'), 'node_modules'],
    extensions: ['*', '.js', '.css', '.styl']
  },
  resolveLoader: {
    // An array of directory names to be resolved to the current directory
    modules: ['node_modules']
  },
  module: {
    rules: [
      {
        test: /\.js$/,
        exclude: /node_modules/,
        loader: ['babel-loader', 'eslint-loader']
      },
      {
        test: /\.(css|styl)$/,
        use: cssConfig
      },
      {
        test: /\.(eot|svg|ttf|woff|woff2)$/,
        loader: 'file-loader'
      }
      // {
      //   test: /\.hbs$/,
      //   loader: 'handlebars-loader'
      // }
      // {
      //   test: /\.(png|jpe?g||gif|svg)$/,
      //   use: [
      //     {
      //       loader: 'file-loader',
      //       options: {
      //         name: '[path][sha256:hash:base64:12].[ext]',
      //         publicPath: 'assets/',
      //         outputPath: 'images/'
      //       }
      //     },
      //     {
      //       loader: 'image-webpack-loader',
      //       options: {
      //         mozjpeg: {
      //           progressive: true,
      //           quality: 65
      //         },
      //         // optipng.enabled: false will disable optipng
      //         optipng: {
      //           enabled: false
      //         },
      //         pngquant: {
      //           quality: '65-90',
      //           speed: 4
      //         },
      //         gifsicle: {
      //           interlaced: false
      //         },
      //         // the webp option will enable WEBP
      //         webp: {
      //           quality: 75
      //         }
      //       }
      //     }
    ]
  },
  devServer: {
    contentBase: path.join(__dirname, 'public'),
    compress: true,
    port: 9000,
    stats: 'errors-only',
    proxy: {
      '/': {
        target: 'http://localhost:8000',
        pathRewrite: {'^/': ''}
      }
    },
    open: false, // Open windows when starting a server
    hot: true // Reload webpage in the browser when files are changed
  },
  plugins: [
    new HtmlWebpackPlugin({
      title: 'Main',
      // minify: {
      //   collapseWhitespace: true
      // },
      hash: true,
      template: path.resolve(__dirname, 'src/views/layouts/main.hbs'),
      filename: 'main.html'
    }),
    new HtmlWebpackPlugin({
      title: 'Admin',
      // minify: {
      //   collapseWhitespace: true
      // },
      hash: true,
      template: path.resolve(__dirname, 'src/views/layouts/admin.hbs'),
      filename: 'admin.html'
    }),
    new ExtractTextPlugin({
      filename: 'style.css',
      allChunks: true,
      publicPath: path.resolve(__dirname, 'public'),
      disable: false
    }),
    new webpack.NamedModulesPlugin(),
    new webpack.HotModuleReplacementPlugin()
  ]
}
