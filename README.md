# รันครั้งแรก
1. ติดตั้ง git และโหนด nodejs ตามลิงค์

- [Git](https://git-scm.com/download/win) (select components: เช็คถูกทั้งหมดตอนติดตั้ง > adjust PATH environment: Use Git from the Windows Command Prompt > นอกนั้นใช้ Default ทั้งหมด)
- [Nodejs](https://nodejs.org/en/download/current/) (current version)
- [MySQL](https://dev.mysql.com/downloads/installer/) (ถ้ามีแล้วไม่ต้อง)
2. เปิด "Git CMD", ติดตั้ง npm (node package management) รัน `npm install npm@latest -g && npm install -g pm2`

# โหลด project
1. ไปที่โฟล์เดอร์ที่ต้องการโหลดโปรเจค > click ขวา > Git Bash here
2. รัน `git clone https://samandriel@bitbucket.org/samandriel/boilerplate-web.git ./test`
1. `cd test`
2. `nmp install` (เฉพาะครั้งแรก)

# Start project
3. รัน server `npm run dev`
4. เข้าหน้าเว็บใน browser `http://localhost:9000`
5. **หยุด server `ctrl + c`**

---
# Stacks
- Backend: Node + Express
- Frontend: Handlebars, Stylus, Webpack, [Bootstrap4](https://getbootstrap.com/docs/4.0/getting-started/introduction/), font-awesome
- Database: MySql

<!-- # ครั้งแรก (สำหรับ windows)
1. ถ้าไม่ได้ติดตั้ง Docker Toolbox ให้ทำการติดตั้งก่อน [DockerToolbox](https://www.docker.com/products/docker-toolbox)

# รัน Project
1. Start Devlopment Environment รัน `docker-compose up`
2. ดูรายการ container `docker-compose ps`
3. `docker exec -it rox-node bash` -->