import passport from 'passport'
import { Strategy } from 'passport-local'
import User from './../models/User.js'

passport.use(new Strategy({
  usernameField: 'email'
}, async (email, password, done) => {
  try {
    const user = await User.findOne({ email })
    if (!user) {
      return done(null, false)
    }
    await user.comparePassword(password, (error, isMatch) => {
      if (error) throw error
      if (!isMatch) {
        return done(null, false)
      }
      return done(null, user)
    })
  } catch (error) {
    done(error, false)
  }
}))
