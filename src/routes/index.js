import express from 'express'
import './user.js'
import './admin.js'

const router = express.Router()

router.get('/', (req, res) => {
  res.render('homepage')
})
router.get('/link', (req, res) => { res.render('link') })
router.get('/link3', (req, res) => { res.render('link3') })

module.exports = router
