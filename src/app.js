import express from 'express'
import morgan from 'morgan'
import routes from './routes/index'
import passport from 'passport'
import bodyParser from 'body-parser'
import cookieParser from 'cookie-parser'
import hbs from 'express-handlebars'
import path from 'path'

// import validator from 'validator'
require('dotenv').config()

const app = express()

app.engine('hbs', hbs({extname: 'hbs', defaultLayout: 'main.html', layoutsDir: path.resolve(__dirname, '../public')}))
app.set('views', path.join(__dirname, 'views'))
app.set('view engine', 'hbs')

// Database Connection
// mongoose.connect(process.env.DB_URI, { useMongoClient: true })
// mongoose.Promise = global.Promise

// Middlewares
app.use(morgan('dev'))
app.use(bodyParser.urlencoded({ extended: false }))
app.use(bodyParser.json())
app.use(cookieParser())
app.use(passport.initialize())
app.use('/', routes)
app.use((err, req, res, next) => {
  res.status(422).send({error: err.message})
})

console.log(__dirname)
// Start server
const port = process.env.PORT || 8000
app.listen(port, () => {
  console.log(`Server running on port ${port}.\nClient running on port 9000.`)
})
